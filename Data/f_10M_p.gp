reset
set terminal postscript eps color solid enhanced 26
#set terminal postscript eps monochrome 26
set output "f_ML10M_P.eps"
set key default
set key out horiz
set key top center

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 1.5
set style fill solid
#set ytics 200
set ylabel "Gflops"
set xtic rotate by 0 scale 0
#set xlabel "#Feature Space Size"


set ytics nomirror
#set y2tics
set tics out
#set autoscale y2
#set y2range [0:*]

plot 'featuresize2.dat' using 2:xtic(1) title "BALS" fill pattern 2 lc rgb "#D55E00", '' using 3 title "Gates" fill pattern 2 lc rgb "#56B4E9"
#, '' using 4 title "BALS-ML20M" fill pattern 1 lc rgb "#CC79A7", '' using 5 title "Gates-ML20M" fill pattern 1 lc rgb "#009E73"
