reset
set terminal postscript eps color solid enhanced 28
set terminal postscript eps monochrome 28
set output "results_P1BATCH_yhr1.eps"
set key default
set key out horiz
set key top center

set grid y

set style line 1  lt 1 lw 16 pt -1 ps 2 lc rgb "#009E73"
set style line 2  lt 2 lw 16 pt -1 ps 2 lc rgb "#CC79A7"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"
set style line 5  lt 5 lw 4 pt 2 ps 2 lc rgb "#999999"
set style line 6  lt 6 lw 4 pt 10 ps 2 lc rgb "#D54E00"
set style line 7  lt 7 lw 4 pt 14 ps 2 lc rgb "#E14900"
set style line 8  lt 8 lw 4 pt 18 ps 2 lc rgb "#C3EE00"

set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 1.5
set style fill solid
#set ytics 200
set ylabel "GFlops"
set xtic rotate by 0 scale 0
set xlabel "DR Batch Number"


set ytics nomirror
#set y2tics
set tics out
#set autoscale y2
#set y2range [0:*]

#plot "results_P1BATCH_yhr1.dat" using 2:xtic(1) title 'Gates-Or' with linespoints ls 1 , '' using 4 title 'Gates-Re' with linespoints ls 2, '' using 3 title 'BALS-Or' fill pattern 2 lc rgb "#0000FF", '' using 5 title 'BALS-Re' fill pattern 2 lc rgb "#D55E00"

plot "results_P1BATCH_yhr1.dat" using 3:xtic(1) title 'BALS-Or' fill pattern 2 lc rgb "#0000FF", '' using 5 title 'BALS-Re' fill pattern 2 lc rgb "#D55E00"
