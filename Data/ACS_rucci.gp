reset
set term postscript eps color solid 30
set output "ACS_rucci.eps"
set grid
unset key

set ylabel "Time-RUCI [s]"

set style line 1  lt 1 lw 6 pt 7 ps 3 lc rgb "#D55E00"
set style line 2  lt 2 lw 6 pt 5 ps 3 lc rgb "#56B4E9"
set style line 3  lt 3 lw 6 pt 3 ps 3 lc rgb "#CC79A7"
set style line 4  lt 4 lw 6 pt 11 ps 3 lc rgb "#009E73"
set style line 5  lt 5 lw 6 pt 9 ps 3 lc rgb "#999999"
set style line 6  lt 6 lw 6 pt 13 ps 3 lc rgb "#C12345"

plot "ACS_rucci.dat" using 2:xtic(1) title 'CDMF' with linespoints ls 1, '' using 3:xtic(1) title 'BALS' with linespoints ls 2, '' using 4:xtic(1) title 'CuMF_CCD' with linespoints ls 3
