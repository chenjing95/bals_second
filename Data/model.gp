reset
set terminal postscript eps color solid enhanced 24
set terminal postscript eps monochrome 24
set output "model.eps"
#set key default
#set key out horiz
#set key top center
set key right

set size 1.5, 1.2

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 1.5
set style fill solid
#set ytics 200
set ylabel "GFlops"
set xtic rotate by -45 scale 0
#set xlabel "#(xblock,yblock,nb,dx,dy)"


set ytics nomirror
#set y2tics
set tics out
#set autoscale y2
#set y2range [0:*]

plot 'model.dat' using 2:xtic(1) title "K20C" fill pattern 2 lc rgb "#D2691E", '' using 3 title "TITAN X" fill pattern 2 lc rgb "#56B4E9"
