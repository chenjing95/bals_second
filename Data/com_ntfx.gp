reset
set term postscript eps color solid 32
set output "com_ntfx.eps"
set grid
unset key

set ylabel "GFlops (NTFX)"

set style line 1  lt 1 lw 6 pt 7 ps 3 lc rgb "#D55E00"
set style line 2  lt 2 lw 6 pt 5 ps 3 lc rgb "#56B4E9"
set style line 3  lt 3 lw 6 pt 3 ps 3 lc rgb "#CC79A7"
set style line 4  lt 4 lw 6 pt 11 ps 3 lc rgb "#009E73"
set style line 5  lt 5 lw 6 pt 9 ps 3 lc rgb "#999999"
set style line 6  lt 6 lw 6 pt 13 ps 3 lc rgb "#C12345"

plot "results_SAC_HPDC_Bigdata.dat" using 4:xtic(1) title 'CuMF' with linespoints ls 3, '' using 2:xtic(1) title 'Gates' with linespoints ls 2, '' using 8:xtic(1) title 'BALS' with linespoints ls 1
