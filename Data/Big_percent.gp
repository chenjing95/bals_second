reset
set terminal postscript eps color solid enhanced 28
set terminal postscript eps monochrome 28
set output "Big_percent.eps"
set style data histogram
set style histogram rowstack
set style fill pattern

set size 1.25, 1.0

#set xtics border in scale 0, 0 nomirror rotate by 90 offset character 0, -6, 0
set boxwidth 0.35 absolute

set yrange [0:100]

set key out horiz
set key top center
#set key outside right top vertical left
set key samplen 2 spacing 1

set ylabel "Step1 BreakDown (%)" font ",30" offset character 2.5,0,0

plot 'Big_percent.dat' using 2:xtic(1) title "Load" fill pattern 2 lc rgb "#6699CC", '' using 3 title "Calculate" fill pattern 2 lc rgb "#FFCC00", '' using 4 title "Store" fill pattern 2 lc rgb "#FF6600"
