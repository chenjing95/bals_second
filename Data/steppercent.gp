reset
set terminal postscript eps color solid enhanced 28
set terminal postscript eps monochrome 28
set output "step.eps"
set style data histogram
set style histogram rowstack
set style fill pattern

set size 1.5, 1.2

#set xtics border in scale 0, 0 nomirror rotate by 90 offset character 0, -6, 0
set boxwidth 0.5 absolute

set yrange [0:100]

set key out horiz
set key top center
#set key outside right top vertical left
set key samplen 2.5 spacing 0.85

set ylabel "ALS Steps BreakDown (%)" font ",30" offset character 2.5,0,0

plot 'steppercent.dat' using 2:xtic(1) title "Step 1" fill pattern 2 lc rgb "#6699CC", '' using 3 title "Step 2" fill pattern 2 lc rgb "#FFCC00", '' using 4 title "Step 3" fill pattern 2 lc rgb "#FF6600"
