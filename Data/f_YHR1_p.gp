reset
set terminal postscript eps color solid enhanced 26
#set terminal postscript eps monochrome 26
set output "f_YHR1_P.eps"
set key default
set key out horiz
set key top center

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 1.5
set style fill solid
set ylabel "Gflops"
set xtic rotate by 0 scale 0
set ytics nomirror
set tics out


plot 'featuresize2.dat' using 8:xtic(1) title "BALS" fill pattern 2 lc rgb "#D55E00", '' using 9 title "Gates" fill pattern 2 lc rgb "#56B4E9"
