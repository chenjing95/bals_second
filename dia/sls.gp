reset
set terminal postscript eps color solid enhanced 38
#set terminal postscript eps monochrome 38
set output "sls.eps"

set xlabel "nonzeros per row"
set ylabel "count of rows (10^5)"

set xrange [1:10000]
set logscale x 10

set ytics ("0" 0, "4" 400000, "8" 800000, "12" 1200000, "16" 1600000)

plot 'sls.txt' using 1:2 title ""
