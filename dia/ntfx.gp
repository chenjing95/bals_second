reset
set terminal postscript eps color solid enhanced 38
#set terminal postscript eps monochrome 38
set output "ntfx.eps"

set xlabel "nonzeros per row"
set ylabel "count of rows (10^2)"

set xrange [1:10000]
set logscale x 10

set ytics ("0" 0, "10" 1000, "20" 2000, "30" 3000, "40" 4000)

plot 'ntfx.txt' using 1:2 title ""
