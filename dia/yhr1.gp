reset
set terminal postscript eps color solid enhanced 38
#set terminal postscript eps monochrome 38
set output "yhr1.eps"

set xlabel "nonzeros per row"
set ylabel "count of rows (10^4)"

set xrange [1:10000]
set logscale x 10

set ytics ("0" 0, "4" 40000, "8" 80000, "12" 120000, "16" 160000, "20" 200000)

plot 'yhr1.txt' using 1:2 title ""
