reset
set terminal postscript eps color solid enhanced 38
#set terminal postscript eps monochrome 38
set output "ml10m.eps"

set xlabel "nonzeros per row"
set ylabel "count of rows (10^2)"

set xrange [1:10000]
set logscale x 10

set ytics ("0" 0, "4" 400, "8" 800, "12" 1200)

plot 'ml10m.txt' using 1:2 title ""
