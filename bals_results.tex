\section{Performance Evaluation} \label{sec_bals_results}
This section reports how well BALS performs. 
We first compare the performance of BALS with the state-of-the-art implementations from various angles.
Then we analyze the performance improvement from data reuse and reordering techniques, and evaluate the performance impact of several parameters in BALS (i.e., $xb$, $yb$, $f$).

\subsection{State-of-the-Art ALS Implementations}
In this section, we describe other three state-of-the-art ALS implementations and 
compare their performance with BALS on K20C, which is shown in Figure~\ref{fig_results_SAC_HPDC_Bigdata}.

Rodrigues et al. introduce a basic ALS implementation in CUDA~\cite{DBLP:conf/sac/RodriguesJD15}, 
where each GPU thread updates a row $x_{u}$ of $\mathbf{X}$ (Equation~\ref{eq:x_u}) or a column $y_{i}$ of $\mathbf{Y}$ (Equation~\ref{eq:y_i}). 
Thus, the implementation has a total of $m$ (or $n$) independent tasks and at most $m$ (or $n$) threads can run concurrently. 
As one GPU thread is used to update a row of the $\mathbf{X}$ matrix, all the temporary data of $\mathbf{Y}^{T}\mathbf{Y}$ is allocated dynamically in the kernel function. 
However, when $f$ becomes large, there is insufficient global memory space remained for dynamic allocation and thus the kernel failed to run. 
Therefore, the implementation does not scale well over the latent factor.
Given that their work is a baseline implementation and only supports $f$=10, we only compare other three ALS implementations in this section.

CuMF uses a thread block to update a row of the $\mathbf{X}$ matrix or a column of the $\mathbf{Y}$ matrix~\cite{DBLP:conf/hpdc/TanCF16}. 
The entire task of calculating $\mathbf{Y}^{T}\mathbf{Y}$ is partitioned into multiple tiles, each sized of $10\times 10$. 
Then CuMF lets each thread work on such a data tile. 
Instead of using a loop to iterate a $10\times 10$ data tile, it fully unrolls the loop and allocates 100 registers to store the temporary results of $smat$. 
Taking $f$=10 as an example, CuMF uses only one thread to calculate the temporary results of $\mathbf{Y}^{T}\mathbf{Y}$. 
But CuMF exploits a specially customized kernel for the case when $f=100$, which leads to a dramatic performance improvement, shown in Figure~\ref{fig_results_SAC_HPDC_Bigdata}.

\begin{figure}[!t]
\centering
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_ml10M.eps}
\label{fig_com_ML10M}}
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_ml20M.eps}
\label{fig_com_ML20M}}
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_ntfx.eps}
\label{fig_com_NTFX}}
\\
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_yhr1.eps}
\label{fig_com_YHR1}}
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_sls.eps}
\label{fig_com_SLS}}
\subfloat{\includegraphics[width=0.165\textwidth]{./Data/com_rucci.eps}
\label{fig_com_RUCI}}
\caption{The performance comparison of three ALS implementations over different latent feature sizes ranging from 10 to 100 on K20C. Note that the performance of Rodrigues' implementation is rather slow and it only supports $f$=10, so we only compare the other three ALS implementations (CuMF, Gates, BALS).}
\label{fig_results_SAC_HPDC_Bigdata}
\end{figure}

Gates et al. present an ALS solver in CUDA~\cite{DBLP:conf/bigdataconf/GatesAKD15}.
They leverage a batched implementation and use a 3D grid of thread blocks, 
($\left\lceil\frac{f}{nb}\right\rceil$, $\left\lceil\frac{f}{nb}\right\rceil$, $batch$).
The product of $\mathbf{Y}^{T}\mathbf{Y}$ is an $f \times f$ matrix for each row of $\mathbf{R}$, and this task is divided into sub-tiles sized of $nb \times nb$. 
Each sub-tile is mapped to a thread block.
The approach moves the corresponding columns of $\mathbf{Y}$ sized of $kb \times f$ 
into shared memory $\mathbf{sY}$ (or $\mathbf{sY^{T}}$) and exploits $(f/nb) \times (f/nb)$ thread blocks to calculate $\mathbf{Y}^{T}\mathbf{Y}$. 
Meanwhile, they use a 2D grid of thread configuration (i.e., each thread block has $dx \times dy$ threads) and allocates a register file sized of $(nb/dx) \times (nb/dy)$ for each thread to save the temporary results.

\begin{figure*}[!t]
\centering
\subfloat{\label{fig_ml10M}\includegraphics[width=0.165\textwidth]{Data/10m_k_ori.eps}}
\subfloat{\label{fig_ml10M_p}\includegraphics[width=0.165\textwidth]{Data/10m_p_ori.eps}}
\subfloat{\label{fig_ml20M}\includegraphics[width=0.165\textwidth]{Data/20m_k_ori.eps}} 
\subfloat{\label{fig_ml20M_p}\includegraphics[width=0.165\textwidth]{Data/20m_p_ori.eps}}
\subfloat{\label{fig_ntfx}\includegraphics[width=0.165\textwidth]{Data/ntfx_k_ori.eps}} 
\subfloat{\label{fig_ntfx_p}\includegraphics[width=0.165\textwidth]{Data/ntfx_p_ori.eps}}
\\
\subfloat{\label{fig_yhR1}\includegraphics[width=0.165\textwidth]{Data/yhr1_k_ori.eps}}
\subfloat{\label{fig_yhR1_p}\includegraphics[width=0.165\textwidth]{Data/yhr1_p_ori.eps}}
\subfloat{\label{fig_sls}\includegraphics[width=0.165\textwidth]{Data/sls_k_ori.eps}}
\subfloat{\label{fig_sls_p}\includegraphics[width=0.165\textwidth]{Data/sls_p_ori.eps}}
\subfloat{\label{fig_rucci}\includegraphics[width=0.165\textwidth]{Data/rucci_k_ori.eps}}
\subfloat{\label{fig_rucci_p}\includegraphics[width=0.165\textwidth]{Data/rucci_p_ori.eps}}
\caption{The performance impact over xb and yb and the performance comparison of BALS and Gates' implementation (labeled on the colorbar) before data reordering on various datasets, where $f$=32, $nb$=16, $dx$=$dy$=4.}
\label{fig_results_xb}
\end{figure*}

Figure~\ref{fig_results_SAC_HPDC_Bigdata} compares our approach with other two state-of-the-art ALS implementations over different feature space sizes (ranging from 10 to 100) on six datasets.  
We see that BALS generally runs the fastest on K20C among the four implementations and Gates' implementation runs the second in five datasets.
It is obvious that when $f$ is small, the performance gap between BALS and Gates' implementation over various datasets is small.
But when $f$ increases, the performance gap becomes more larger.
This is because a larger f will achieve more data reuses. 
Overall, our BALS implementation runs, on average, 1.26$\times$, 1.46$\times$, 1.30$\times$, 2.19$\times$, 2.30$\times$ and 2.25$\times$ faster than Gates' for \texttt{Movielens 10M}, \texttt{Movielens 20M}, \texttt{Netflix}, \texttt{Yahoomusic R1}, \texttt{Sls} and \texttt{Rucci}, respectively.
In particular, BALS achieves the maximum speedup of 4.38$\times$ on $f$=100 when compared with Gates' implementation on \texttt{YahooMusic R1}.

\subsection{Performance Comparison with SGD and CCD++}
The alternating least squares algorithm has been demonstrated to be effective in solving matrix factorization.
There are also two other algorithms to factorize a rating matrix: cyclic coordinate decent and its variant (CCD++) and stochastic gradient descent (SGD).
Here we compare BALS with these three state-of-the-art implementations: CDMF~\cite{DBLP:conf/cf/YangFCWTL17}, cuMF\_CCD~\cite{Nisa:2017:PCG:3038228.3038240} for CCD++ and cuMF\_SGD for SGD~\cite{DBLP:conf/hpdc/XieTFL17}.
Since cuMF\_SGD now only supports $f$=128, we compare them in such a case.
Our experimental results show that BALS outperforms cuMF\_SGD with an average speedup of 5.60$\times$ for the six datasets on K20C.

\begin{table}[!t]
\caption{The average performance speedups of BALS over CDMF and CuMF\_CCD}
\label{tbl_ACS}
\begin{center}
\scalebox{0.7}{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|} \hline
                    & ML10M     & ML20M  & NTFX     & YHR1      & SLS     & RUCI   \\ \hline
BALS vs. CDMF       & 7.10 X	& 6.89 X & 9.37 X   & 15.26 X   & 3.04 X  & 21.64 X  \\ \hline
BALS vs. CuMF\_CCD  & 2.75 X 	& 2.31 X & 4.04 X   & 9.09 X    & 1.79 X  & 11.39 X \\ \hline
\end{tabular}}
\end{center}
\end{table}

Figure~\ref{fig_ACS} shows the performance comparison between CDMF, cuMF\_CCD and BALS over different latent feature sizes on K20C.
Overall, when keeping the same prediction accuracy, i.e., RMSE, we observe that BALS completely outperforms CDMF and CuMF\_CCD over different latent feature size on six datasets. 
Table~\ref{tbl_ACS} lists the average speedups of BALS over CDMF and CuMF\_CCD. 
We obtain different level of performance speedups on six datasets.
In particular, we see that CDMF and CuMF\_CCD scale linearly, 
while the performance of BALS fluctuates slightly due to the different best observed parameters chosen for different $f$.


\begin{figure}[!b]
\centering
\subfloat{\label{fig_ACS_ml10M}\includegraphics[width=0.167\textwidth]{Data/ACS_ml10m.eps}}
\subfloat{\label{fig_ACS_ml20M}\includegraphics[width=0.167\textwidth]{Data/ACS_ml20m.eps}}
\subfloat{\label{fig_ACS_NTFX}\includegraphics[width=0.167\textwidth]{Data/ACS_ntfx.eps}} 
\\
\subfloat{\label{fig_ACS_yhr1}\includegraphics[width=0.167\textwidth]{Data/ACS_yhr1.eps}} 
\subfloat{\label{fig_ACS_sls}\includegraphics[width=0.167\textwidth]{Data/ACS_sls.eps}}
\subfloat{\label{fig_ACS_rucci}\includegraphics[width=0.167\textwidth]{Data/ACS_rucci.eps}}
\caption{The performance comparison of CDMF, CuMF\_CCD and BALS over various latent feature sizes on K20C, where we keep the same RMSE in this case.} 
\label{fig_ACS}
\end{figure}

\subsection{Vacant Tiles and Segments Analysis}

The rating matrix $\mathbf{R}$ is divided into multiple 2D tiles in BALS. 
However, there exists a large amount of vacant tiles and segments due to the sparsity of data distribution.
Thus, we introduce the inspection mechanism of checking vacant tiles and/or vacant segments to save time.
We count the percentage of vacant tiles and segments of the six datasets and measure the performance benefits of skipping them in BALS when $f$=32.
The performance numbers are shown in Table~\ref{tbl_percent}.
Note that here we differentiate the vacant tiles and vacant segments.
In other words, those segments of vacant tiles are excluded from vacant segments.
We observe that these vacant tiles and vacant segments occupy a large percentage in rating matrices, with an average 35.88\% of vacant tiles and 46.98\% of vacant segments before reordering in the six datasets.
As a consequence, we obtain various levels of performance improvement when skipping these vacancy.
\texttt{Rucci} achieves the highest performance enhancement (60.37\%), while \texttt{Netflix} has no improvement when using the inspection machnism.
We argue that this is because the original \texttt{Netflix} is a denser matrix among these datasets (see the sparsity of Table~\ref{tbl_datasets}).
The percentages of vacant tiles and vacant segments are much lower than these of other datasets.


\begin{table}[!h]
\caption{The percentage of vacant tiles and segments and performance gain by skipping them}
\label{tbl_percent}
\begin{center}
\scalebox{0.6}{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|} \hline
              & \multicolumn {3} {|c|} {Before Reordering} & \multicolumn {3} {|c|} {After Reordering}  \\ \hline
Dataset       & Vacant Tiles & Vacant Segments & Benefits  & Vacant Tiles & Vacant Segments & Benefits  \\ \hline
ML10M         & 43.67\%      & 49.44\%         & 11.88\%   & 87.92\%      & 7.82\%         & 16.22\%    \\ \hline
ML20M         & 18.30\%      & 63.85\%         & 7.51\%    & 27.81\%      & 57.36\%         & 0.25\%    \\ \hline
NTFX          & 2.38\%       & 24.10\%         & 0         & 0.59\%       & 69.59\%         & 0.15\%    \\ \hline
YHR1          & 52.28\%      & 45.73\%         & 23.65\%   & 81.13\%      & 17.31\%         & 7.13\%    \\ \hline
SLS           & 26.35\%      & 71.85\%         & 9.04\%    & 19.35\%      & 79.07\%         & 13.31\%   \\ \hline
RUCI          & 72.32\%      & 26.92\%         & 60.37\%   & 72.95\%      & 26.29\%         & 41.60\%   \\ \hline
\end{tabular}}
\end{center}
\end{table}


\begin{figure*}[!t]
\centering
\subfloat{\label{fig_re_ml10M}\includegraphics[width=0.165\textwidth]{Data/10m_k_re.eps}}
\subfloat{\label{fig_re_ml10M_p}\includegraphics[width=0.165\textwidth]{Data/10m_p_re.eps}}
\subfloat{\label{fig_re_ml20M}\includegraphics[width=0.165\textwidth]{Data/20m_k_re.eps}} 
\subfloat{\label{fig_ml20M_re_p}\includegraphics[width=0.165\textwidth]{Data/20m_p_re.eps}}
\subfloat{\label{fig_re_ntfx}\includegraphics[width=0.165\textwidth]{Data/ntfx_k_re.eps}} 
\subfloat{\label{fig_ntfx_re_p}\includegraphics[width=0.165\textwidth]{Data/ntfx_p_re.eps}}
\\
\subfloat{\label{fig_re_yhR1}\includegraphics[width=0.165\textwidth]{Data/yhr1_k_re.eps}} 
\subfloat{\label{fig_re_yhR1_p}\includegraphics[width=0.165\textwidth]{Data/yhr1_p_re.eps}} 
\subfloat{\label{fig_re_sls}\includegraphics[width=0.165\textwidth]{Data/sls_k_re.eps}}
\subfloat{\label{fig_re_sls_p}\includegraphics[width=0.165\textwidth]{Data/sls_p_re.eps}}
\subfloat{\label{fig_re_rucci}\includegraphics[width=0.165\textwidth]{Data/rucci_k_re.eps}}
\subfloat{\label{fig_re_rucci_p}\includegraphics[width=0.165\textwidth]{Data/rucci_p_re.eps}}
\caption{The performance impact over xb and yb and the performance comparison between BALS and Gates' implementation (labeled on the colorbar) after data reordering on various datasets, where $f$=32, $nb$=16, $dx$=$dy$=4.} 
\label{fig_results_re_xb}
\end{figure*}


\subsection{Performance Impact of xb and yb} \label{xyb}
In BALS, $xb$ and $yb$ denote the number of rows and columns in a specific 2D tile, respectively.
The tile size $xb$ and $yb$ have a dramatic performance impact on BALS, which directly determines the total number of data reuses.
When $yb$ ($xb$) is fixed for a tile, the number of data reuses of the tile will increase over $xb$ ($yb$) theoretically.
The best performance of BALS involves the balance between the benefits from column reuse and the computing costs because of increased number of rows in a specific tile.

Figures~\ref{fig_results_xb} shows the performance impact of $xb$ and $yb$ on various datasets.
We also make a performance comparison with the fastest state-of-the-art implementation~\cite{DBLP:conf/bigdataconf/GatesAKD15} (labeled on the colorbar of the heatmaps).
We observe that BALS generally outperforms Gates' implementation for various datasets on both K20C and TITAN X. 
Specifically, BALS's performance changes gradually with $xb$ and $yb$, 
and the best performance on various datasets appears on the top left corner of the heatmaps. 
Such performance behaviors indicate how to select the best configurations.  
Also, we notice that there exists a dramatic performance change for \texttt{Movielens 20M} and \texttt{YahooMusic R1}, 
when $xb$ ranges from 256 to 1280.
This is because these two datasets have a different data distribution from the others. 
The first few rows of the two datasets have so few nonzeros that the performance benefits from tiling do not dominate the calculation. 

We observe from Figure~\ref{fig_results_xb} that BALS obtains an performance improvement on various datasets
when compared to Gates' implementation. 
For \texttt{YahooMusic R1}, BALS obtains the highest performance improvement (99\%) on the TITAN X among the six datasets when $f$=32, 
while using the same configuration on this dataset can achieve only half of that on K20C.
For the \texttt{Sls} and \texttt{Rucci} datasets, we achieve a performance improvement of 49.2\% and 29.8\% on TITAN X and K20C, respectively, when compared to Gates' implementation.

\subsection{Performance Impact of Data Reordering}
BALS exploits the data reordering techinique to exploit data locality and makes the best of data reuse technique.
In this way, the nonzeros of rating matrix cluster as much as possible.
Table~\ref{tbl_percent} illustrates that the proportions of vacant tiles and segments change dramatically after reordering.
We find that, when compared with the proportions before reordering, 
the percentages of vacant tiles in most datasets are larger, 
while that of vacant segments drops except \texttt{Netflix} and \texttt{Sls}.
The reason is that, when the matrix nonzeros cluster as much as possible,
the scattered vacant segments without reordering form new vacant tiles,
%other areas of the rating matrix are vacant tiles, 
thus the number of vacant tiles rises and the number of vacant segments decreases after reordering.


We analyse the performance impact of the data reordering technique by comparing between Figures~\ref{fig_results_xb} and~\ref{fig_results_re_xb}. 
Figure~\ref{fig_results_re_xb} shows that the performance comparison of BALS and Gates' implementation after reordering. 
We see that the best tile configuration changes by reordering rows or columns. 
For \texttt{Movielens 10M}, for example, BALS achieves its best performance when $xb$=1280, $yb$=384 before reordering, whereas it appears at $xb$=256, $yb$=384 after reordering on K20C. 
We also note that the best observed tile configurations differ across datasets. 
The best performance is achieved at $xb$=2816, $yb$=384 on K20C before reordering and at $xb$=768, $yb$=384 after reordering for \texttt{Movielens 20M}. 
%\texttt{Netflix} achieves the same best performance at xb=1280, yb=384 before and after reordering. 
%Similarly, \texttt{YahooMusic R1} achieves best performance at xb=5376, yb=384 and xb=768, yb=384 after data reordering. 
The experimental results also indicate that achieving the best observed performance without data reordering needs more rows in a tile so as to take advantage of column reuses.
At the same time, our data reordering technique brings a further performance enhancement compared with Gates' implementation.
%~\cite{DBLP:conf/bigdataconf/GatesAKD15}.
For example, \texttt{Movielens 20M} achieves a 53\% performance enhancement on K20C.

\begin{table}[!b]
\caption{The performance improvements of data reordering}
\label{tbl_Datareordering}
\begin{center}
\scalebox{0.80}{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|} \hline
              & ML10M   & ML20M  & NTFX & YHR1 & SLS    & RUCI   \\ \hline
K20C          & 23.68\%		& 11.72\%	& 9.77\%   & 6.26\%        & 0.64\% & 0.33\%  \\ \hline
TITAN X       & 19.87\% 	& 11.92\%	& 8.96\%   & 5.87\%        & 1.29\% & 2.02\%  \\ \hline
\end{tabular}}
\end{center}
\end{table}

\begin{figure*}[!t]
\centering
\subfloat{\label{fig_f1}\includegraphics[width=0.245\textwidth]{Data/feature.eps}}
%\subfloat[ML10M(TITAN X)]{\label{fig_f5}\includegraphics[width=0.245\textwidth]{Data/f_ML10M_P.eps}}
\subfloat{\label{fig_f2}\includegraphics[width=0.245\textwidth]{Data/feature_ML20M.eps}}
%\subfloat[ML20M(TITAN X)]{\label{fig_f6}\includegraphics[width=0.245\textwidth]{Data/f_ML20M_P.eps}}
\subfloat{\label{fig_f3}\includegraphics[width=0.245\textwidth]{Data/feature_NTFX.eps}} 
%\subfloat[NTFX(TITAN X)]{\label{fig_f7}\includegraphics[width=0.245\textwidth]{Data/f_NTFX_P.eps}}
\subfloat{\label{fig_f4}\includegraphics[width=0.245\textwidth]{Data/feature_YHR1.eps}} 
%\subfloat[YHR1(TITAN X)]{\label{fig_f8}\includegraphics[width=0.245\textwidth]{Data/f_YHR1_P.eps}}
\caption{The performance impact of different feature space size ranging from 8 to 96 on BALS over two GPU platforms, where $xb$, $yb$ exploit the best observed parameters from Section~\ref{xyb}.} 
\label{fig_f}
\end{figure*}

Table~\ref{tbl_Datareordering} shows the performance improvements of data reordering technique over the six datasets in BALS when compared with the performance without data reordering.
% over , by 11.72\% on K20C and 11.92 \% on TITAN X over \texttt{Movielens 20M}, by 9.77\% on K20C and 8.96\% on TITAN X over \texttt{Netflix} and by 6.26\% on K20C and 5.87\% on TITAN X over \texttt{YahooMusic R1} on K20C.
We obtain different levels of performance improvements in these datasets, with the average 8.73\% and 8.32\% increase on K20C and TITAN X, respectively.
The highest performance improvement is achieved in \texttt{Movielens 10M}, increasing by 23.68\% on K20C and 19.87\% on TITAN X.
However, we observe that the data reordering technique brings a rather small performance improvement for the \texttt{Sls} and \texttt{Rucci} datasets.
%, which are chosen from florida sparse matrix collection that arise in real applications. 
Our analysis on the matrices shows that their shape is more structured and the nonzeros distribute more evenly across rows or columns
%and not particularlly scattered when compared with 
than the other four datasets.

%\subsection{Performance Impact of DRbat} \label{sec:seg_drbat}
%In BALS, $DRbat$ represents the number of batches which using data reuse technique. Our tiling technique cannot bring a performance improvement once there is insufficient column reuses within a tile. We sort all batches according to the total amount of column reuses accumulated by tiles. Then the data reuse technique is applied on the top $DRbat$ batches to make the best of columns reuses.

%Figure~\ref{fig_p1batch} shows the performance impact of $DRbat$ ranging from 1 to 10 before and after data reordering, where $f$=32, $nb$=16, $dx$=$dy$=4. We observe that, in most cases, our implementation scales linearly up to 10 batches for both before and after data reordering. For example, the execution time of BALS is same as that of Gates' when $DRbat$ is 8 on \texttt{YahooMusic R1} before and after reordering, respectively. Also, BALS achieves the similar performance to that of Gates' when $DRbat$=3 on \texttt{Netflix} and \texttt{Movielens 20M} before reordering. After row reordering, BALS achieves the same performance as Gates' when $DRbat$=7 on \texttt{Netflix} and $DRbat$=9 on \texttt{Movielens 20M}. The experimental results illustrate that BALS has more data reuses after data reordering and thus can take full advantage of data reuses. This is due to the fact that the nonzeros move towards the top left-part of the sparse matrix.


\subsection{Performance Impact of Feature Space Size}
The first step of $\mathbf{Y}^{T}\mathbf{Y}$ is to load corresponding column vectors each sized of $f$ from $\mathbf{Y}$ to $\mathbf{sY}$.
Thus, when $f$ becomes larger, more data need to be loaded in the computing process.
When using the data reuse technique in ALS, we can avoid $redundancy$$\times$$f$ repeated data loads. 
In this way, BALS can avoid a large amount of data loads and make the best of the data reuse technique when $f$ is large.

Figure~\ref{fig_f} shows the performance impact of the feature space size ($f$ ranging from 8 to 96) on the two platforms.
%and the performance comparison with fastest Gates' implementation.
%We apply the same task partitioning strategy and threads configuration as Gates' implementation so as to guarantee the fairness of comparison.
%For example when $f$=96, we set $nb$=48, $dx$=$dy$=8.
We see that the performance trends of BALS on the two platforms are almost same and the performance gaps between these two platforms are significant.
There exists slightly fluctuation between $f$=8 to 56.
But after $f$=56, it is obvious that BALS has a dramatic performance improvements on all datatsets.
%when $f$ is small, the performance gap between BALS and Gates' implementation over various datasets is narrow.
%But when $f$ increases, the performance gap becomes more wider.
%It is obvious that BALS generally outperforms Gates' implementation over most of $f$.
Specifically, we find that BALS runs up to 2.08$\times$ and 3.72$\times$ faster than Gates' implementation on K20C and TITAN X, respectively.


