\section{Related Work} \label{sec_related_work}
In this section, we discuss four \textit{matrix factorization} algorithms for recommender systems and their implementations on multi-cores, many-cores and distributed platforms. 
As stated in~\cite{DBLP:journals/computer/KorenBV09}, matrix factorization is regarded as the most successful realization of latent factor models in recommender systems. 
When factorizing a rating matrix, ALS (Alternating Least Squares), SGD (Stochastic Gradient Descent), CCD (Cyclic Coordinate Decent) and SVD (Singular Value Decomposition) are the four most commonly used techniques. 

\textbf{The ALS solver.} GraphLab implements ALS by distributing matrix on multiple machines while the matrix is large, which results in heavy cross-node traffic and pretty high network bandwidth~\cite{DBLP:journals/pvldb/LowGKBGH12}. 
\texttt{Spark MLlib} leverages partial matrix replication to parallelize ALS~\cite{DBLP:journals/corr/MengBYSVLFTAOXX15}. 
\texttt{CuMF}, a CUDA-based matrix factorization library, implements memory-optimized ALS to solve very large-scale MF by using a variety set of techniques to maximize the performance on either single or multiple GPUs. These techniques include smart access of sparse data leveraging GPU memory hierarchy, using data parallelism in conjunction with model parallelism, minimizing the communication overhead between computing units, and utilizing a novel topology-aware parallel reduction scheme~\cite{DBLP:conf/hpdc/TanCF16}. Gates et al. formulate ALS as a mix of cache-optimized algorithm-specific kernels and batched Cholesky factorization~\cite{7275187}, and accelerate it on GPUs and multi-threaded CPUs~\cite{DBLP:conf/bigdataconf/GatesAKD15}. 
Zhou et al. introduce a new parallel algorithm ALS-WR (weighted regulation) for large-scale problems by using parallel Matlab on a linux cluster~\cite{DBLP:conf/aaim/ZhouWSP08}.

\textbf{The CCD solver.} Yu et al. propose a scalable and efficient method CCD++ which has a different update sequence from the basic CCD and updates rank-one factors one by one. 
The algorithm has two versions of parallelization on different machines: one version for multi-core shared memory systems and the other for distributed systems~\cite{DBLP:conf/icdm/YuHSD12}. 
Recently Nisa et al. improve the CCD++ method on GPUs with loop fusion and tiling~\cite{Nisa:2017:PCG:3038228.3038240}.
Yang et al. present an efficient and portable CDMF solver on modern multi-core and many-cores~\cite{DBLP:conf/cf/YangFCWTL17}.
In particular, they balance the factorization loads by re-organizing the non-zero entries of rating matrices.  

\textbf{The SGD solver.} 
Paine et al. present an asynchronous SGD to speed up the neural network training on GPUs~\cite{DBLP:journals/corr/PaineJYLH13}. 
In~\cite{DBLP:conf/nips/AgarwalD11, DBLP:conf/nips/ZinkevichWSL10}, the authors propose a delayed update scheme and a bootstrap aggregation scheme to speed up SGD. 
\texttt{HogWild} uses a lock-free approach to parallelize SGD, which is shown to be more efficient than the delayed update scheme~\cite{DBLP:conf/nips/RechtRWN11}. 
\texttt{DSGD} (Distribute SGD) partitions the ratings matrix into several blocks and updates a set of independent blocks concurrently~\cite{DBLP:conf/kdd/GemullaNHS11}. 
Rashid Kaleem et al. show that the parallel SGD can run efficiently on GPU, and their implementation on GPU is comparable to a 14-thread CPU implementation~\cite{DBLP:conf/ppopp/KaleemPP15}. 
Jinoh et al. propose \texttt{MLGF-MF}, which is robust to skewed matrices and runs efficiently on block-storage devices (e.g., SSD disks) as well as shared-memory platforms. 
The implementation leverages \textit{multi-level grid file} to partition the rating matrix and minimizes the cost of scheduling parallel SGD updates on the partitioned regions~\cite{DBLP:conf/kdd/OhHYJ15}.
\texttt{CuMF\_SGD}, a CUDA-enabled SGD solution for large-scale matrix factorization problems, uses two workload scheduling schemes (\textit{batch-Hogwild!} and \textit{wavefront-update}) and a partitioning scheme to utilize multiple GPUs. 
At the same time, the authors address the well-known convergence issue when parallelizing SGD~\cite{DBLP:journals/corr/XieTFL16}. 
\texttt{Factorbird} uses a parameter server in order to scale models that exceed the memory of an individual machine, 
and employs a lock-free \textit{Hogwild!-style} learning with a special partitioning scheme to drastically reduce conflicting updates~\cite{DBLP:journals/corr/SchelterSZ14}. 
Sallinen et al. explore serveral modern parallelization methods of SGD on a shared memory system~\cite{DBLP:conf/ipps/SallinenSSSR16}. 
In particular, they present a scalable, communication-avoiding implementation of SGD and demonstrate near-linear scalability on a system with 14 cores. 

\textbf{The SVD solver.} 
Matrix factorization models map both users and items to a joint latent factor space of dimentionality $f$, such that user-item interactions can be modeled as inner products in that space.
Therefore, the recommendation problem is how to compute a mapping of items and uses to factor vectors~\cite{DBLP:journals/computer/KorenBV09, DBLP:jourals/SIAM/lingfei15}.
In the collaborative filtering domain, singular value decomposition (SVD) is a well-established technique of identifying latent feature factors.
However, the conventional SVD is often unapplicable in matrix factorization of the recommendation field due to the high percentage of missing entries in the sparse user-item matrix.
When the matrix is incomplete, it is not possible to achieve the factoring task.
Moreover, overfitting would occur if we address the sparse matrix carelessly.
Therefore, we need an approach that can simply ignore the missing ratings in the sparse matrix, modeling directly the observed ratings.
To this end, researchers have performed intensive research to improve the applicability of SVD in collaborative filtering.
For example, in~\cite{DBLP:chih-chao}, Chih-chao proposed four variants of SVD to solve large-scale matrix of collaborative filtering instead of the conventional SVD, 
including incomplete incremental learning, complete incremental learning, complete incremental learning, batch learning with a momentum, SVD with biases.
He observed that complete incremental learning which updates feature values after scaning a single training score of R, is the best choice for collaborative filtering with millions of training instances.
The method minimizes the object function and addresses the negative gradients for each user and item according to each non-zero elements of the R matrix per time.
Therefore, it has nnz (i.e., total number of non-zero elements in R) iterations.






