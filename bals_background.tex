\section{Background} \label{sec_bals_background}
In this section, we describe ALS-based matrix factorization and then analyze this algorithm.

\subsection{ALS-based Matrix Factorization}
The basic idea for matrix factorization is to minimize the regularized squared error on the observed ratings to learn the factors,

\begin{equation}
L(X, Y)=\sum_{u,i\in \Omega} (r_{ui}-x_{u}^{T}y_{i})^{2}+\lambda (|x_{u}|^{2}+|y_{i}|^{2}), 
\label{eq:obj_function}
\end{equation}
where $\Omega$ are the known nonzero ratings of $\mathbf{R}$, and $x_{u}^{T}$ are the $u^{th}$ row vectors of the matrix $\mathbf{X}$, $y_{i}$ are $i^{th}$ column vectors of matrix $\mathbf{Y}$, the constant $\lambda$ is the regularized coefficient to avoid over-fitting. Therefore, the key to solve this problem is to find approaches of getting the matrices $\mathbf{X}$ and $\mathbf{Y}$. 

Among all the matrix factorization techniques, \textit{alternating least squares} (ALS) is regarded as an efficient one. 
The minimization principle of alternating least squares is \textit{to keep one fixed while calculating the other}: we fix the $\mathbf{Y}$ matrix to calculate the $\mathbf{X}$ matrix so as to get vectors $x_{u}$, and vice versa. 
In this way, the problem becomes a quadratic function. The procedure iterates until it converges. 
First, we minimize the equation over $\mathbf{X}$ while fixing $\mathbf{Y}$, and the function becomes 

\begin{equation}
L(X)=\sum_{i \in \Omega_{u}}(r_{ui}-x_{u}^{T}y_{i})^{2}+\lambda |x_{u}|^{2}
\label{eq:obj_function_x}
\end{equation}

By calculating the partial derivative of $x_{u}$ in Function~\ref{eq:obj_function_x} and letting the partial derivative equal zero, we can obtain 

\begin{equation}
x_{u}=(Y^{T}Y+\lambda I)^{-1}Y^{T}r_{u}, 
\label{eq:x_u}
\end{equation}
where $I$ is the unit matrix ranked $f$, and $r_{u}$ is the $u^{th}$ rows of $\mathbf{R}$. In the same way, we can obtain $y_{i}$

\begin{equation}
y_{i}=(X^{T}X+\lambda I)^{-1}X^{T}r_{i}.
\label{eq:y_i}
\end{equation}

\subsection{Algorithm Analysis} \label{subsec:als_analysis}
The ALS algorithm consists of three steps when factorizing the rating matrix.
Taking $x_{u}$ as an example, the three steps are (S1) $Y^{T}Y+\lambda I$, (S2) $Y^{T}r_{u}$, and (S3) solving the linear system. 

As for S1, calculating $\mathbf{Y}^{T}\mathbf{Y}$ requires $nnz_{i}\times f\times(f+1)/2$ \textit{multiply-add} operations for a row of $\mathbf{R}$, 
where $nnz_{i}$ denotes the number of nonzero entries in the current row. 
Therefore, the total compute cost is $nnz\times f\times (f+1)$, 
where $nnz$ denotes the total number of nonzero elements in $\mathbf{R}$. 
In terms of memory footprint, we need a matrix $smat$ (sized of $f \times f$) to store the results of $\mathbf{Y}^{T}\mathbf{Y}$ when updating a row. 
Thus, the total memory footprint for $m$ rows is $m\times f \times f$.
Calculating S2 requires $nnz_{i} \times f$ \textit{multiply-add} operations when updating the $i^{th}$ row of $\mathbf{R}$. 
Thus, the total computing cost of S2 is $nnz \times f \times 2$. 
This step needs a vector $svec$ sized of $f$ to store the results of $\mathbf{Y}^{T}\mathbf{r_{u}}$,
and thus the total memory footprint for $m$ rows is $m\times f$.
After obtaining $smat$ and $svec$, the \textit{cholesky} decomposition method is exploited to solve the linear system $smat \cdot x_{u}=svec$ (S3). 
And the time complexity of the cholesky method is O($f^{3}$) for updating a row of $\mathbf{R}$.

Figure~\ref{fig_step_ALS} shows that $\mathbf{Y}^{T}\mathbf{Y}$ is the most time-consuming step,
which takes on average 90\% of the end-to-end time over various latent factors. 
Therefore, this step is the focus of our work. 
Similarly, calculating $\mathbf{X}^{T}\mathbf{X}$ takes the most time when we update an item vector ($y_{i}$).

\begin{figure}[!t]
\centering
\includegraphics[width=0.38\textwidth]{./Data/step.eps}
\caption{The illustration about the percentage of three ALS steps on \texttt{Netflix} dataset. The three steps are (S1) $\mathbf{Y}^{T}\mathbf{Y}+\lambda I$, (S2) $\mathbf{Y}^{T}r_{u}$, and (S3) solving the linear system.}
\label{fig_step_ALS}
\end{figure}
