import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

data = np.genfromtxt('ori2048_netflix.txt',delimiter=',')

a = data[:,0]
b = data[:,1]
c = data[:,2]

X=np.unique(a)
Y=np.unique(b)

Z=c.reshape(len(X),len(Y))

df=pd.DataFrame(Z, columns=[i for i in range(len(Y))])
ax = sns.heatmap(df, cmap = "Blues")

#cbar=ax.collections[0].colorbar
#cbar.ax.tick_params(labelsize=16)

plt.draw()

data2 = np.genfromtxt('2048_netflix.txt',delimiter=',')

a2 = data2[:,0]
b2 = data2[:,1]
c2 = data2[:,2]

X2=np.unique(a2)
Y2=np.unique(b2)

Z2=c2.reshape(len(X2),len(Y2))

df2=pd.DataFrame(Z2, columns=[i for i in range(len(Y))])
ax2 = sns.heatmap(df2, cmap = "Reds")

#cbar2=ax2.collections[0].colorbar
#cbar2.ax2.tick_params(labelsize=30)

plt.title('Movies Popularity',fontsize=38)
plt.ylabel('User Group ID', fontsize=32)
plt.xlabel('Movie ID', fontsize=32)
plt.yticks(rotation=0)
plt.draw()

plt.show()
