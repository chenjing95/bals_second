import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

data = np.genfromtxt('2048_netflix.txt',delimiter=',')

a = data[:,0]
b = data[:,1]
c = data[:,2]

X=np.unique(a)
Y=np.unique(b)

Z=c.reshape(len(X),len(Y))

df=pd.DataFrame(Z, columns=[i for i in range(len(Y))])
ax = sns.heatmap(df, cmap = "Reds")

cbar=ax.collections[0].colorbar
cbar.ax.tick_params(labelsize=30)

plt.title('Movies Popularity',fontsize=38)
plt.ylabel('User Group ID', fontsize=32)
plt.xlabel('Movie ID', fontsize=32)
plt.yticks(rotation=0)
plt.savefig('ori.eps', format='eps', dpi=1200)
plt.draw()
plt.show()
