import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset

data = np.genfromtxt('ori2048_netflix.txt',delimiter=',')

a = data[:,0]
b = data[:,1]
c = data[:,2]

X=np.unique(a)
Y=np.unique(b)

Z=c.reshape(len(X),len(Y))

df=pd.DataFrame(Z, columns=[i for i in range(len(Y))])
ax = sns.heatmap(df, cmap = "Reds")

cbar=ax.collections[0].colorbar
cbar.ax.tick_params(labelsize=16)

plt.title('Column Reuse Times (Original NTFX)',fontsize=26)
plt.ylabel('#Rowblock-id', fontsize=22)
plt.xlabel('#Column-id', fontsize=22)

plt.show()
