reset
set terminal postscript eps color solid enhanced 35
set terminal postscript eps monochrome 35
set size 2.0,1.3
set output "NTFX.eps"
unset key 

set style line 1  lt -1 lw 1 pt 0 ps 1
set style data histogram
#set style histogram cluster gap 3
#set style fill solid
set ylabel "Popularity"
set xlabel "Movie ID"
unset xtics
set title "Netflix Movie Popularity (after reodering)"
plot 'NTFX.txt' using 2:xtic(1) fill pattern 1 lc rgb "#6699cc"
