\begin{figure}[!b]
\centering
\includegraphics[width=0.5\textwidth]{./dia/NTFX_ORI.eps}
\caption{
The illustration about movie popularity among users over the \texttt{Netflix} dataset (blue bars). 
The red bars demonstrate the popularity of each movie by sorting the matrix according to the number of ratings. }
\label{fig_column_reuse}
\end{figure}

\section{Motivation} \label{sec_bals_motivation}
In this section, we start with the observation from the real-world datasets
and illustrate the issues of the previous ALS implementations, which motive our work. 

\subsection{Data Reuse}
According to our statistics on various recommender datasets, we observe that 
%the majority of users has a rating to a small proportion of items, which are more prevalent than others. 
\emph{there exist popular items which have been rated by the majority of users and, at the same time, there exist users who have given a score to most, if not all, items}.
%For instance, in Netflix \% users approximately give a mark to top 20\% popular movies and \% users give a mark to \% prevalent musics in Yahoomusic R1.
To visualize this trend, 
%we organize the rows of the \texttt{Netflix} dataset in the form of row blocks, and
% we take the Netflix dataset as an example and 
we count the total number of ratings (a.k.a. \textit{popularity}) for each movie in the Netflix dataset.
Figure~\ref{fig_column_reuse} shows how popular each movie is. 
We see that some movies are watched by many users (i.e., the movie has a lot of ratings in one column),
 while others by only a few (i.e., there are very few ratings for the corresponding movie). 
This is why the diagram shows the vertical sparse lines when the movies are popular. 

%The results enlighten us that we could exploit the statistics feature that a large amount of users give a mark to a a small proportion of popular items.
%has much columns data reuse and batching strategy of \texttt{Gates}~\cite{DBLP:conf/bigdataconf/GatesAKD15} for others.
In alternating least squares, updating a user vector ($\mathbf{x}_{u}$) needs to load the data elements (i.e., $nnz_{i}$ column vectors each sized of $f$) from $\mathbf{Y}$ 
according to the column indexes of the items that the user has rated.
When updating a neighbouring user vector ($\mathbf{x}_{u+1}$), the same column vectors of $\mathbf{Y}$ can be used again. 
This occurs when two nonzero entries from different rows of $\mathbf{R}$ share the same column index. 
Thus, we can avoid the movement of one column vector in $\mathbf{Y}$, so as to save the memory bandwidth. 
%and utimately improve the overall factorization performance.
Our observation from Figure~\ref{fig_column_reuse} has shown that this is a common case in recommendation datasets.
However, the previous implementations let different threads (or thread blocks) work on distinct user vectors~\cite{DBLP:conf/bigdataconf/GatesAKD15, DBLP:conf/hpdc/TanCF16}.
The already-loaded column vectors from $\mathbf{Y}$ are either evited out of caches or be manually overwritten in scratch-pad memories. 
This is the same when we calculate item vectors. 
In this work, we aim to exploit such a data feature to avoid redundant movements.
%The previous implementations run the updating work independently, which ignore the data features of dataset statistics, cause the enormous data loading and decline the performance as well.
%However, if the obtained statistics are exploited and users are clustered into a group according to the ratings to same items, we could use \textit{data reuse} and decrease the amount of data loading to update all users/items.

\begin{figure}[!h]
\centering
\subfloat[]{\includegraphics[width=0.23\textwidth]{./dia/Data_reuse.eps}
\label{reuse1}}
\subfloat[]{\includegraphics[width=0.255\textwidth]{./dia/Data_reuse2.eps}
\label{reuse2}}
\caption{The illustration of data reuse and the comparison before and after data reordering, where the row block size of the matrix is 3. Matrix $\mathbf{Y}$ denotes the collection of item vectors and $\mathbf{sY}$ is the cached content used to update each user vector.}
\label{fig_Data_reuse}
\end{figure}

If there exist two nonzeros of distinct rows sharing the same column index, 
updating the corresponding row vectors requires the same column vector from $\mathbf{Y}$.
%we will load the same column vector from $\mathbf{Y}$.
Actually, we can avoid one extra data movement of $f$ data elements when updating the row vectors simultaneously. 
We regard this as a \textit{data reuse}.
Analytically, a data reuse occurs once Equation~\ref{eq:data_reuse} holds.
\begin{small}
\begin{equation}
\label{eq:data_reuse}
col\_idx[row\_ptr[start_{a}+i]]=col\_idx[row\_ptr[start_{b}+j]],
\end{equation}
\end{small}
where $a$ and $b$ are two rows in a row block of $\mathbf{R}$, $i$ denotes the $i^{th}$ nonzero of row $a$ and $j$ denotes the $j^{th}$ nonzero of row $b$.
In BALS, we organize all rows into groups, each of which is defined as a \textit{row block}. 
Figure~\ref{reuse1} illustrates the concept of \textit{data reuse}, 
where we group six rows into two row blocks each with three rows.
% (R0, R1, R2; R3, R4, R5). 
%and each rowblock contains three rows.
The first nonzero of R0 has the same column index (C1) with the first nonzero of R1.
% which denotes a time of data reuse.
Thus, we only have to load the column vector once (vector 1 shaded in yellow). 
In the same way, the column vectors (vectors 4 and 5) can be reused in the second row block.
%Therefore, the loading tasks of the matrix only need to move data size of 8$\times$f instead of 11$\times$f.
%In summary, the observations motivte us to investigate how to take the advantage of \textit{data reuse} for a minimization of duplicated data movements.

\begin{figure}[!t]
\centering
\includegraphics[width=0.39\textwidth]{./Data/Big_percent.eps}
\caption{The percentage of three steps in calculate $\mathbf{Y}^{T}\mathbf{Y}$ of the implementation in~\cite{DBLP:conf/bigdataconf/GatesAKD15} over various datasets. Computing $\mathbf{Y}^{T}\mathbf{Y}$ consists of three steps: (1) loading data into shared memory, (2) calculating $\mathbf{sY}^{T}\mathbf{sY}$, (3) storing results back to global memory.}
\label{fig_Big_percent}
\end{figure}

As we have shown in Section~\ref{subsec:als_analysis}, computing $\mathbf{Y}^{T}\mathbf{Y}$ takes about 90\% of the entire factorization.
While taking a closer look, we know that calculating $\mathbf{Y}^{T}\mathbf{Y}$, again, consists of three steps: 
(1) loading data into shared memory, 
(2) calculating $\mathbf{sY}^{T}\mathbf{sY}$, 
(3) storing results back to global memory.
Figure~\ref{fig_Big_percent} shows the breakdown of the three steps.
We note that the data loading process (step 1) takes around 50\% of calculating $\mathbf{Y}^{T}\mathbf{Y}$,
while the remaining two steps consume the other half. 
By reducing the data loading time, we can efficiently speed up the entire factorization process. 
This motivates us to investigate the exploitation of \textit{data reuse} with an aim to minimizing duplicated data movements. 

\subsection{Data Reordering}

\begin{figure}[!b]
\centering
\subfloat[ml10m]{\includegraphics[width=0.165\textwidth]{./dia/ml10m.eps}
\label{ml10m}}
\subfloat[ml20m]{\includegraphics[width=0.165\textwidth]{./dia/ml20m.eps}
\label{ml20m}}
\subfloat[ntfx]{\includegraphics[width=0.165\textwidth]{./dia/ntfx.eps}
\label{ntfx}}
\\
\subfloat[yhr1]{\includegraphics[width=0.165\textwidth]{./dia/yhr1.eps}
\label{yhr1}}
\subfloat[sls]{\includegraphics[width=0.165\textwidth]{./dia/sls.eps}
\label{sls}}
\subfloat[ruci]{\includegraphics[width=0.165\textwidth]{./dia/rucci.eps}
\label{rucci}}
\caption{The nonzero distribution of rows of target datasets.}
\label{fig_Data_distribution}
\end{figure}

Figure~\ref{fig_Data_distribution} shows the nonzero distribution of the target datasets. 
Each dot of the histogram represents the number of rows with a certain number of nonzeros. 
We observe that the number of nonzeros varies from very few to tens of thousand, 
e.g., there are 197,277 rows, each with one nonzero for the YahooMusic R1 dataset.
Thus, we understand that most elements of the sparse matrix are zeros.  
Due to the data sparsity, there actually exists many scattered \textit{vacant tiles} and/or \textit{vacant row segments} that are all zeros in specific areas of the rating matrix.  
Dealing with such vacant segments comes at a cost.  
If a specific 2D tile is vacant, we only have to check it once and then skip it in the outer iteration.
But if there are several vacant row segments in the tile, we have to enumerate the inner iteration to detect these vacant row segments for multiple times, 
which takes more time than checking a vacant tile. 
For this, we present a \textit{data reordering} technique.
This can be achieved by sorting the rows and columns of $\mathbf{R}$ in the decending order of nonzeros. 
After reordering, many scattered vacant row segments cluster to form new vacant tiles, which declines the total cost of checking separate vacant row segments.
We argue that applying this technique to the dataset could further enhance data locality and make the best of benefits of data reuse.
As a result, most of nonzeros move towards the top-left corner of the matrix as much as possible.

The red part of Figure~\ref{fig_column_reuse} shows how the ratings distribute for each movie in Netflix after data reordering.
We see that the popular movies move towards left and leave the unpopular movies on the right.
In this way, we can draw the nonzeros to be closer, decrease the number of vacant row segments and create more vacant tiles.
%, which decline the checking cost of vacant row segments.  
Figure~\ref{reuse2} illustrates the data distribution and data reuse of rating matrix after exploiting the data reordering technique.
It shows that most nonzeros move towards the top-left corner after reordering and the total data reuse times increase by 1.
Therefore, it is significant to perform data reordering on the rating matrix so as to both enhance data reuse and decrease the cost of vacant segments. 


