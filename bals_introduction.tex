\section{Introduction} \label{sec_recsys_introduction}

\textit{Matrix factorization} has been taken as one of the most successful realizations of latent factor models 
and thus becomes a dominant methodology used in the machine learning fields such as collaborative filtering recommender systems~\cite{DBLP:journals/computer/KorenBV09}.
Its task is to fill in the missing entries of a partially observed matrix.
The input of matrix factorization is an incomplete relation matrix $\mathbf{R}(m \times n)$. 
In the context of recommender systems, $m$ and $n$ denote the number of users and items, respectively. 
Due to the sparsity of $\mathbf{R}$, matrix factorization maps both users and items to a joint factor space of dimensionality $f$ (i.e., \textit{latent feature}), so that predicting unknown ratings can be estimated by the inner products of two vectors, $x_{u}$ of matrix $\mathbf{X}(m \times f)$ and $y_{i}$ of matrix $\mathbf{Y}(n\times f)$,

\begin{equation}
r_{ui}=x_{u}  {y_{i}}^{T},
\end{equation}
where $x_{u}$ denotes the extent of user's interest on items, 
$y_{i}$ denotes the extent to which the item owns these factors, and $r_{ui}$ denotes an entry of $\mathbf{R}$. 
The key of the problem is to obtain $x_{u}$ and $y_{i}$ so that $\mathbf{R}\approx \mathbf{XY}^{T}$.
Figure~\ref{fig_example} illustrates the process of matrix factorization,
where $m$=$n$=$4$ and $f$=$2$. 

\begin{figure}[!h]
\centering
\includegraphics[width=0.41\textwidth]{./dia/Example.eps}
\caption{An example of sparse matrix factorization $\mathbf{R}\approx \mathbf{X}\mathbf{Y^{T}}$, 
where there are 4 users and 4 items, and $f$=$2$. The rating matrix $\mathbf{R}$ has 9 ratings out of 16.  
%$m$=$n$=$4$ and $f$=$2$. 
Our aim with matrix factorization is to calculate $\mathbf{X}$ and $\mathbf{Y}$ from the known ratings. 
After obtaining $\mathbf{X}$ and $\mathbf{Y}$, the unknown ratings of $\mathbf{R}$ can be estimated by $\mathbf{XY^{T}}$.}
\label{fig_example}
\end{figure}

So far, there has been a large amount of work dedicated to the design of fast and scalable methods for large-scale matrix factorization problems~\cite{DBLP:conf/aaim/ZhouWSP08, DBLP:journals/computer/KorenBV09, DBLP:journals/jmlr/TakacsPNT09, DBLP:journals/jgo/NguyenH17, DBLP:conf/icassp/SimsekliDBRMC17, DBLP:conf/mod/KayaBOG17}. 
However, matrix factorization over extremely sparse matrices (e.g., recommender datasets) is a still challenging issue~\cite{DBLP:journals/siammax/SchaubTDD17, DBLP:journals/tist/TaoTLG17}.
Among the matrix factorization techniques, \textit{alternating least squares} (ALS) has been proved to be an effective one~\cite{DBLP:journals/computer/KorenBV09}. 
Compared to \textit{stochastic gradient descent} (SGD)~\cite{DBLP:conf/kdd/GemullaNHS11, DBLP:conf/icdm/TeflioudiMG12}, the ALS algorithm is not only inherently parallel, but can incorporate implicit ratings~\cite{DBLP:journals/computer/KorenBV09}. 
Nevertheless, the ALS algorithm involves parallel sparse matrix manipulation~\cite{DBLP:phd/phdthesisliu} which is challenging to achieve high performance due to imbalanced workload~\cite{DBLP:conf/ICS/2015}, random memory access and task dependency. 
This particularly holds when parallelizing and optimizing ALS on modern multi-cores and many-cores~\cite{DBLP:conf/ipps/ChenFLTCY17}. 
To address the issue, researchers have investigated various solutions. 
Among them, Rodrigues et al. present a CUDA-based ALS implementation on GPU, which is claimed to run faster than the implementation on a multi-core CPU~\cite{DBLP:conf/sac/RodriguesJD15}. 
Tan et al. provide a CUDA-based matrix factorization library (\texttt{cuMF}), which uses various techniques to maximize the performance on one or multiple GPUs~\cite{DBLP:conf/hpdc/TanCF16}. 
Also, Gates et al. propose a multi-core CPU implementation and a GPU ALS solver 
for implicit feedback datasets, 
which attains good performance through an algorithm-specific kernel and is, thus far, the fastest implementation among these ALS solvers on modern GPUs~\cite{DBLP:conf/bigdataconf/GatesAKD15}.

In spite of these optimizing efforts, the training speed of parallel sparse matrix factorization based on the ALS algorithm does not still reach its optimum. 
%\textcolor{blue}{These previous work mainly focused on the optimization of hardware resources exploitation.
%However, they ignore the relation of latent information of datasets with the computing process of the algorithm.
%In our observation, we find that there exists many users who have ratings for the same item, and the number of these type of items are enormous in these datasets.
%When computing each user vector of user matrix, it is imperitive to load corresponding item vectors from the item matrix, which are indexed by nonzeros item number of this row in rating matrix.
%Thus, based on the observation, we can decrease the massive loading time of these same items vectors from the item matrix and improve the performance.}
Although the previous works have used a fine-grained technique to exploit the hierarchical resources on modern many-cores, 
they ignore the internal social property embedded in the real-world datasets~\cite{DBLP:conf/mod/KayaBOG17, DBLP:conf/hpdc/TanCF16, DBLP:conf/ipps/ChenFLTCY17}. 
In short, we observe that there exist many users who have ratings for the same item, and the number of such type of items are enormous in these datasets.
When computing $\mathbf{X}$ or $\mathbf{Y}$, the previous approaches let a thread or a block of threads work on a user vector ($\mathbf{x_{u}}$) or an item vector ($\mathbf{y_{i}}$).
But updating two user vectors might need the same item vectors from $\mathbf{Y}$.
As a result, two threads would load the same vector twice, leading to the redundant data movments and a waste of memory bandwidth. 
The motivating observations are further detailed in Section~\ref{sec_bals_motivation}. 

In this work, we propose an efficient implementation for parallel sparse matrix factorization, \textbf{BALS}\footnote{\textbf{BALS} is a name short for ``a \textbf{B}locked \textbf{ALS} Solver for Matrix Factorization''. The source code of the BALS implementation is online available: \url{https://github.com/jingchen95/BALS}.}, based on the alternating least squares algorithm. 
In BALS, we develop a data reuse technique based on the observations, which can avoid repeated data loads of row or column vectors from global memory to shared momory and thus improve the data-moving efficiency.
To enable the data reuse in parallel sparse matrix, we propose a new blocked storage format for sparse matrices by partitioning a matrix into 2D tiles.
In order to take full advantage of data locality and enhance the benefits of our data reuse technique, a data reordering technique is proposed in BALS by sorting rows and columns in the decending order according to nonzeros.
%BALS consists of a new storage format for sparse matrices and its relevant ALS implementation. 
%Specifically, we develop a data reuse technique and a data reordering technique through a series of algorithm analysis and experiments on ALS. 
%We have an obeservation that, by exploiting data reuse, we can avoid the repeated data loads of row or column vectors from global memory to shared momory. 
%Furthermore, we present a data reordering technique 
%By reordering rows and columns according to nonzeros, we aim to take full advantage of data locality. 
The experimental results show that we demonstrate a better performance than the state-of-the-art implementations on modern many-core platforms with various real-world datasets.
Overall, our implementation generally runs faster than Gates' implementation over different latent feature sizes, with an maximum speedup of 2.08$\times$ and 3.72$\times$ on K20C and TITAN X, respectively.
Furthermore, we also analyze the performance of BALS with other matrix fatorization algorithm implementations,
seeing that our approach completely outperforms CDMF~\cite{DBLP:conf/cf/YangFCWTL17}, cuMF\_CCD~\cite{Nisa:2017:PCG:3038228.3038240} for CCD++ and cuMF\_SGD~\cite{DBLP:conf/hpdc/XieTFL17} for SGD over different latent feature sizes on K20C.
Meanwhile, the extra 8.73\% and 8.32\% performance improvements over various real-world datasets are achieved in BALS by exploiting the data reordering technique on K20C and TITAN X, respectively. 
Note that BALS is applicable to other many-core platforms, such as AMD GPUs, although it is implemented in CUDA on NVIDIA GPUs.

To summarize, we make the following contributions. 
\begin{itemize}

\item We present a data reuse technique to avoid redundant data movements across memory hierarchies. 
To guarantee this, we develop a new compressed storage format for sparse matrices by partitioning a matrix into 2D tiles.

\item We propose a data reordering technique to decrease the processing time of vacant segments and maximize the benefits of data locality.

\item By incorporating our approach, We develop an efficient ALS implementation with CUDA, which is able to make the most of the on-chip architecture resources.

\item We evaluate how our algorithm performs compared with the baseline implementations on NVIDIA GPUs with various real-world datasets, and demonstrate that our approach obtains a superior performance.
\end{itemize}
 
The remainder of this paper is organized as follows. 
Section~\ref{sec_bals_background} describes the background of matrix factorization and the ALS algorithm.
Section~\ref{sec_bals_motivation} illustrates our observations and motivations. 
We present our approach in Section~\ref{sec_recsys_approach} and evaluate it in Section~\ref{sec_bals_results}. 
Section~\ref{sec_recsys_setup} introduces the experimental platforms and six real-world datasets. 
Section~\ref{sec_related_work} lists the related work and Section~\ref{sec_conclusion} concludes our work.  
